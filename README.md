# Technical Testing Inosoft
#### Requirements :
1. mongodb 4.4
2. php 7.3 | newer
3. postman
4. composer

## INSTALATION & HOW TO USE API
- **git clone https://gitlab.com/portofolio6/technical-test-inosoft.git**
- **composer install**
- **rename .env.example to .env**
- **setting .env**<br>
    DB_CONNECTION=mongodb
    MONGO_DB_HOST=127.0.0.1
    MONGO_DB_PORT=27017
    MONGO_DB_DATABASE=nama_db
    MONGO_DB_USERNAME=
    MONGO_DB_PASSWORD=
<br>
- **php artisan key:generate**
- **php artisan jwt:generate**
- **php artisan migrate**
- **php artisan serve**
- **Akses REST API** :
    1. Register User baru terlebih dahulu dengan cara hit route Register yang ada di list rest api di bawah.
    2. Kemudian generete Bearer Token dengan route login yang ada di list rest api.
    3. Copy Hasil generate token ke bagian token di menu Authorization yang ada di postman dengan type Bearer Token.
    4. Kemudian hit url route yang di inginkan, untuk examplenya ada di bagian collection postman.


## LIST REST API
- **AUTH**
    - **Register** : POST root/api/auth/register
    request :
    ```json
    {
        "name":"test",
        "email":"test@inosoft.com",
        "password":"testing1",
        "password_confirmation":"testing1"
    }
    ```
    - **Login** : POST root/api/auth/login
    request :
    ```json
    {
        "email":"test@inosoft.com",
        "password":"testing1"
    }
    ```
- **KENDARAAN**
    - **Add Kendaraan jenis Mobil** : POST root/api/kendaraan/store
    request :
    ```json
    {
        "tahun_keluar":"2021",
        "warna":"pink",
        "harga":2000000,
        "jenis":"mobil",
        "mesin":"AZ-124",
        "kapasitas_penumpang":10,
        "tipe":"off road",
        "stok":20
    }
    ```
    - **Add Kendaraan jenis Motor** : POST root/api/kendaraan/store
    request :
    ```json
    {
        "tahun_keluar":"2021",
        "warna":"pink",
        "harga":2000000,
        "jenis":"motor",
        "mesin":"AZ-123",
        "type_suspensi":"manual",
        "type_transmisi":"manual",
        "stok":20
    }
    ```
- **TRANSAKSI**
    - **Add Penjualan** : POST root/api/penjualan/store
    request :
    ```json
    {
        "id_kendaraan":"61d32938d80b22a8ff5c2c43",
        "qty":10
    }
    ```
    - **List laporan penjualan by id kendaraan** : GET root/api/penjualan/list/{id_kendaraan}
    - **List laporan penjualan by id kendaraan** : GET root/api/penjualan/jenis-kendaraan/{jenis}
- **STOK**
    - **Listo Stok kendaraan** : GET root/api/kendaraan/stok 
    - **Listo Stok by id kendaraan** : GET root/api/kendaraan/stok/{id_kendaraan} 

### COLLECTION POSTMAN
**URL COLLECTION** : https://www.postman.com/winter-moon-472918/workspace/inosoft/collection/7353404-54d18224-438b-4c7f-b402-f21b13e43c97

## UNIT TESTING
- untuk menjalankan unit testing di laravel silahkan run command ini **"./vendor/bin/phpunit"** di terminal anda.



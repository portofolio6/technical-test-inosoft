<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKendaraansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kendaraans', function ($collection) {
            $collection->id();
            $collection->string('tahun_keluar');
            $collection->string('warna');
            $collection->NumberDecimal('harga');
            $collection->string('jenis');
            $collection->string('mesin')->unique();
            $collection->string('type_suspensi');
            $collection->string('type_transmisi');
            $collection->string('tipe');
            $collection->NumberInt('kapasitas_penumpang');
            $collection->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kendaraans');
    }
}

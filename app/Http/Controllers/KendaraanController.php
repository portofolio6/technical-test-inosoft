<?php

namespace App\Http\Controllers;

use App\Http\Traits\ApiResponse;
use App\Repository\Kendaraan\QueryKendaraanRepository;
use App\Repository\Stok\QueryStokRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class KendaraanController extends Controller
{
    use ApiResponse;
    protected $queryStokRepository;
    protected $queryKendaraanRepository;
    public function __construct(QueryStokRepository $queryStokRepository, QueryKendaraanRepository $queryKendaraanRepository)
    {
        $this->stok = $queryStokRepository;
        $this->kendaraan = $queryKendaraanRepository;
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'tahun_keluar'      =>  'required|string',
            'warna'             =>  'required|string',
            'harga'             =>  'required|numeric',
            'jenis'             =>  'required|in:motor,mobil',
            'mesin'             =>  'required|string',
        ]);

        if ($validation->fails()) {
            return $this->error(null, $validation->errors(), 422);
        }

        try {
            $kendaraanByMesin = $this->kendaraan->kendaraanByMesin($request);
            if (!empty($kendaraanByMesin)) {
                $updateStok = $this->stok->updateStok($kendaraanByMesin->_id, $request->stok);
                $kendaraanByMesin->stok = $updateStok;
                $data = $kendaraanByMesin;
            } else {
                $kendaraan = $this->kendaraan->addKendaraan($request);
                $stok = $this->stok->addStok($request, $kendaraan->_id);
                $kendaraan->stok = $stok;
                $data = $kendaraan;
            }
            return $this->success($data, 'Data kendaraan berhasil di tambahkan');
        } catch (\Throwable $e) {
            return $this->error($e->getMessage(), 'Data kendaraan gagal di tambahkan', 409);
        }
    }
}

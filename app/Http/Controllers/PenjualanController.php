<?php

namespace App\Http\Controllers;

use App\Http\Traits\ApiResponse;
use App\Repository\Stok\QueryStokRepository;
use App\Repository\Transaksi\QueryTransaksiRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PenjualanController extends Controller
{
    use ApiResponse;
    protected $queryTransaksiRepository;
    protected $queryStokRepository;

    public function __construct(QueryTransaksiRepository $queryTransaksiRepository, QueryStokRepository $queryStokRepository)
    {
        $this->stok = $queryStokRepository;
        $this->penjualan = $queryTransaksiRepository;
    }

    public function getPenjualanByIdKendaraan($id_kendaraan)
    {
        $transaksi = $this->penjualan->listPenjualanByIdKendaraan($id_kendaraan);
        if (empty($transaksi)) {
            return $this->error(null, 'List penjualan kendaraan gagal ditemukan', 404);
        }
        return $this->success($transaksi, 'List penjualan kendaraan berhasil ditemukan');
    }

    public function getPenjualanByJenis($jenis)
    {
        $transaksi = $this->penjualan->listPenjualanByJenis($jenis);
        if (empty($transaksi)) {
            return $this->error(null, 'List penjualan by jenis kendaraan gagal ditemukan', 404);
        }
        return $this->success($transaksi, 'List penjualan by jenis kendaraan berhasil ditemukan');
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'id_kendaraan'      =>  'required|exists:kendaraans,_id',
            'qty'               =>  'required|numeric',
        ]);

        if ($validation->fails()) {
            return $this->error(null, $validation->errors(), 422);
        }

        try {
            $penjualan = $this->penjualan->addTransaksi($request);
            $subStok = $this->stok->subStok($request);
            $mutasiStok = $this->stok->addMutasiStok($penjualan, $subStok->_id);
            $penjualan->mutasi_stok = $mutasiStok;
            return $this->success($penjualan, 'Data penjualan berhasil di tambahkan');
        } catch (\Throwable $e) {
            return $this->error($e->getMessage(), 'Data penjualan gagal di tambahkan', 409);
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Traits\ApiResponse;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Repository\Auth\QueryAuthRepository;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    use ApiResponse;
    protected $queryAuthRepository;

    public function __construct(QueryAuthRepository $queryAuthRepository)
    {
        $this->queryAuth = $queryAuthRepository;
        // $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    public function register(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name'      =>  'required|string',
            'email'     =>  'required|email|unique:users,email',
            'password'  =>  'required|confirmed|min:8'
        ]);

        if ($validation->fails()) {
            return $this->error(null, $validation->errors(), 422);
        }

        try {
            $data = $this->queryAuth->register($request);
            return $this->success($data, 'Register user baru berhasil');
        } catch (\Throwable $e) {
            return $this->error($e->getMessage(), 'Register user baru gagal', 401);
        }
    }

    public function login(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'email'     =>  'required|string',
            'password'  =>  'required|string'
        ]);

        if ($validation->fails()) {
            return $this->error(null, $validation->errors(), 422);
        }
        $credentials = $request->only(['email', 'password']);
        $data = $this->queryAuth->login($credentials);
        if (empty($data)) {
            return $this->error(null, 'Unauthorized, Wrong Email or Password!', 401);
        }
        return $this->jwtResponse($data);
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Traits\ApiResponse;
use App\Repository\Stok\QueryStokRepository;
use Illuminate\Http\Request;

class StockController extends Controller
{
    use ApiResponse;
    protected $queryStokRepository;

    public function __construct(QueryStokRepository $queryStokRepository)
    {
        $this->stok = $queryStokRepository;
    }

    public function index()
    {
        $stok = $this->stok->getListStok();
        if (empty($stok)) {
            return $this->error(null, 'List stok kendaraan gagal ditemukan', 404);
        }
        return $this->success($stok, 'List stok kendaraan berhasil ditemukan');
    }

    public function getStokByIdKendaraan($id_kendaraan)
    {
        $stok = $this->stok->getStokByIdKendaraan($id_kendaraan);
        if (empty($stok)) {
            return $this->error(null, 'Stok kendaraan gagal ditemukan', 404);
        }
        return $this->success($stok, 'Stok kendaraan berhasil ditemukan');
    }
}

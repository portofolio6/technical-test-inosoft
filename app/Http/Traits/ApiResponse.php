<?php

namespace App\Http\Traits;

trait ApiResponse
{
    public function success($data, $message, $code = 200, $status = "true")
    {
        return response()->json([
            'success'    =>  $status,
            'code'      =>  $code,
            'message'   =>  $message,
            'data'      =>  $data
        ], $code);
    }

    public function error($data, $message, $code = 500, $status = "false")
    {
        return response()->json([
            'success'    =>  $status,
            'code'      =>  $code,
            'message'   =>  $message,
            'data'      =>  $data
        ], $code);
    }

    public function jwtResponse($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * env('JWT_TTL')
        ]);
    }
}

<?php

namespace App\Repository\Stok;

use App\Models\MutasiStok;
use App\Models\Stok;

class QueryStokRepository implements StokRepository
{
    public function addStok($request, $id_kendaraan)
    {
        $stok = new Stok;
        $stok->id_kendaraan    =   $id_kendaraan;
        $stok->stok_awal       =   $request->stok;
        $stok->stok_sisa       =   $request->stok;
        $stok->save();
        return $stok;
    }

    public function updateStok($id_kendaraan, $stok)
    {
        $updateStok = Stok::where('id_kendaraan', $id_kendaraan)->first();
        $totalStok = $updateStok->stok_sisa + $stok;
        $updateStok->stok_sisa = $totalStok;
        $updateStok->save();
        return $updateStok;
    }

    public function subStok($request)
    {
        $subStok = Stok::where('id_kendaraan', $request->id_kendaraan)->first();
        $totalStok = $subStok->stok_sisa - $request->qty;
        $subStok->stok_sisa = $totalStok;
        $subStok->save();
        return $subStok;
    }

    public function addMutasiStok($penjualan, $id_stok)
    {
        $mutasiStok = new MutasiStok;
        $mutasiStok->id_stok            =   $id_stok;
        $mutasiStok->no_transaksi       =   $penjualan->no_transaksi;
        $mutasiStok->jenis_transaksi    =   "penjualan";
        $mutasiStok->qty                =   $penjualan->qty;
        $mutasiStok->save();
        return $mutasiStok;
    }

    public function getListStok()
    {
        $stok = Stok::all();
        return $stok;
    }

    public function getStokByIdKendaraan($id_kendaraan)
    {
        $stok = Stok::with('detail_stok')->where('id_kendaraan', $id_kendaraan)->first();
        return $stok;
    }
}

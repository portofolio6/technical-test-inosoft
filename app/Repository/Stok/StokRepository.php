<?php

namespace App\Repository\Stok;

interface StokRepository
{
    public function addStok($request, $id_kendaraan);
    public function updateStok($id_kendaraan, $stok);
    public function subStok($request);
    public function addMutasiStok($penjualan, $id_stok);
    public function getListStok();
    public function getStokByIdKendaraan($id_kendaraan);
}

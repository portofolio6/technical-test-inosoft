<?php

namespace App\Repository\Auth;

interface AuthRepository
{
    public function register($request);
    public function login($request);
}

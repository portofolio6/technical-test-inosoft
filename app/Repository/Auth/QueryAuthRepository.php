<?php

namespace App\Repository\Auth;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

class QueryAuthRepository implements AuthRepository
{
    public function register($request)
    {
        $user = new User;
        $user->name         =   $request->name;
        $user->email        =   $request->email;
        $user->password     =   app('hash')->make($request->password);
        $user->save();
        return $user;
    }

    public function login($credentials)
    {
        if ($token = Auth::attempt($credentials)) {
            return $token;
        }
    }
}

<?php

namespace App\Repository\Transaksi;

use App\Models\Kendaraan;
use App\Models\Penjualan;
use Carbon\Carbon;

class QueryTransaksiRepository implements TransaksiRepository
{
    public function addTransaksi($request)
    {
        $dateNow = Carbon::now()->format('Y-m-d');
        $countTransaksi = Penjualan::where('created_at', $dateNow)->count();
        $transaksi = new Penjualan();
        $transaksi->id_kendaraan    =   $request->id_kendaraan;
        $transaksi->no_transaksi    =   $dateNow . '_INOSOFT_' . str_pad($countTransaksi + 1, 5, 0, STR_PAD_LEFT);
        $transaksi->qty             =   $request->qty;
        $transaksi->save();
        return $transaksi;
    }

    public function listPenjualanByIdKendaraan($id_kendaraan)
    {
        $transaksi = Penjualan::where('id_kendaraan', $id_kendaraan)->get();
        return $transaksi;
    }

    public function listPenjualanByJenis($jenis)
    {
        $kendaraan = Kendaraan::select(['_id'])->where('jenis', $jenis)->get();
        $id_kendaraan = array();
        foreach ($kendaraan as $value) {
            array_push($id_kendaraan, $value->_id);
        }

        $transaksi = Penjualan::whereIn('id_kendaraan', $id_kendaraan)->get();
        return $transaksi;
    }
}

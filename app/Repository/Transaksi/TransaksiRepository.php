<?php

namespace App\Repository\Transaksi;

interface TransaksiRepository
{
    public function addTransaksi($request);
    public function listPenjualanByIdKendaraan($id_kendaraan);
    public function listPenjualanByJenis($jenis);
}

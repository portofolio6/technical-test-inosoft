<?php

namespace App\Repository\Kendaraan;

interface KendaraanRepository
{
    public function addKendaraan($request);
    public function kendaraanByMesin($request);
}

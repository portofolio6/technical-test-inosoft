<?php

namespace App\Repository\Kendaraan;

use App\Models\Kendaraan;

class QueryKendaraanRepository implements KendaraanRepository
{
    public function addKendaraan($request)
    {
        $kendaraan = new Kendaraan;
        $kendaraan->tahun_keluar         =   $request->tahun_keluar;
        $kendaraan->warna                =   $request->warna;
        $kendaraan->harga                =   $request->harga;
        $kendaraan->jenis                =   $request->jenis;
        $kendaraan->mesin                =   $request->mesin;
        if ($request->jenis == "mobil") {
            $kendaraan->tipe                 =   $request->tipe;
            $kendaraan->kapasitas_penumpang  =   $request->kapasitas_penumpang;
        } elseif ($request->jenis == "motor") {
            $kendaraan->type_suspensi        =   $request->type_suspensi;
            $kendaraan->type_transmisi       =   $request->type_transmisi;
        }
        $kendaraan->save();
        return $kendaraan;
    }

    public function kendaraanByMesin($request)
    {
        $kendaraan = Kendaraan::where('mesin', $request->mesin)->where('jenis', $request->jenis)->first();
        return $kendaraan;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class Stok extends Eloquent
{
    use HasFactory;
    protected $connection = 'mongodb';
    protected $collection = 'stoks';
    protected $guarded = [];

    public function detail_stok()
    {
        return $this->hasMany(MutasiStok::class, 'id_stok', '_id');
    }
}

<?php

use App\Http\Controllers\UserController;
use App\Http\Controllers\KendaraanController;
use App\Http\Controllers\PenjualanController;
use App\Http\Controllers\StockController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'auth'], function () {
    Route::post('/register', [UserController::class, 'register']);
    Route::post('/login', [UserController::class, 'login']);
});

Route::group(['middleware' => 'jwt.verify'], function () {

    Route::group(['prefix' => 'kendaraan'], function () {
        Route::post('/store', [KendaraanController::class, 'store']);
        Route::get('/stok', [StockController::class, 'index']);
        Route::get('/stok/{id_kendaraan}', [StockController::class, 'getStokByIdKendaraan']);
    });

    Route::group(['prefix' => 'penjualan'], function () {
        Route::post('/store', [PenjualanController::class, 'store']);
        Route::get('/list/{id_kendaraan}', [PenjualanController::class, 'getPenjualanByIdKendaraan']);
        Route::get('/jenis-kendaraan/{jenis}', [PenjualanController::class, 'getPenjualanByJenis']);
    });
});

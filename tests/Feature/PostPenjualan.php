<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PostPenjualan extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_successful_penjualan()
    {
        $transaksi = [
            "id_kendaraan"  =>  "61d32938d80b22a8ff5c2c43",
            "qty"           =>  10
        ];

        $this->json('POST', 'api/penjualan/store', $transaksi, ['Accept' => 'application/json'])
            ->assertStatus(200);
    }

    public function test_required_fields_for_kendaraan()
    {
        $this->json('POST', 'api/penjualan/store', ['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertJson([
                "success"   => "false",
                "code"  => 422,
                "message"   => [
                    "id_kendaraan" => ["The id_kendaraan field is required."],
                    "qty" => ["The qty field is required."],
                ],
                "data"  => null
            ]);
    }
}

<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_required_fields_for_registration()
    {
        $this->json('POST', 'api/auth/register', ['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertJson([
                "success"   => "false",
                "code"  => 422,
                "message"   => [
                    "name"  => ["The name field is required."],
                    "email" => ["The email field is required."],
                    "password"  => ["The password field is required."]
                ],
                "data"  => null
            ]);
    }

    public function test_repeat_password()
    {
        $userData = [
            "name" => "testing",
            "email" => "testing@inosoft.com",
            "password" => "testing1"
        ];

        $this->json('POST', 'api/auth/register', $userData, ['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertJson([
                "success" => "false",
                "code" => 422,
                "message" => [
                    "password" => ["The password confirmation does not match."]
                ],
                "data" => null
            ]);
    }

    public function test_duplicate_email()
    {
        $userData = [
            "name" => "testing",
            "email" => "testing@inosoft.com",
            "password" => "testing1",
            "password_confirmation" => "testing1"
        ];

        $this->json('POST', 'api/auth/register', $userData, ['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertJson([
                "success" => "false",
                "code" => 422,
                "message" => [
                    "email" => ["The email has already been taken."]
                ],
                "data" => null
            ]);
    }

    public function test_successful_registration()
    {
        $userData = [
            "name" => "testing",
            "email" => "testing90@inosoft.com",
            "password" => "testing2",
            "password_confirmation" => "testing2"
        ];

        $this->json('POST', 'api/auth/register', $userData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure([
                "success",
                "code",
                "message",
                "data" => [
                    'name',
                    'email',
                    'created_at',
                    'updated_at',
                    '_id',
                ]
            ]);
        $this->assertTrue(true);
    }

    public function test_must_enter_email_and_password()
    {
        $this->json('POST', 'api/auth/login')
            ->assertStatus(422)
            ->assertJson([
                "success"   => "false",
                "code"  => 422,
                "message"   => [
                    "email" => ["The email field is required."],
                    "password"  => ["The password field is required."]
                ],
                "data"  => null
            ]);
    }

    public function test_successful_login()
    {
        $loginData = ['email' => 'testing@inosoft.com', 'password' => 'testing1'];

        $this->json('POST', 'api/auth/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure([
                "access_token",
                "token_type",
                "expires_in"
            ]);

        $this->assertAuthenticated();
    }
}

<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PostKendaraan extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_required_fields_for_kendaraan()
    {
        $this->json('POST', 'api/kendaraan/store', ['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertJson([
                "success"   => "false",
                "code"  => 422,
                "message"   => [
                    "tahun_keluar"  => ["The tahun keluar field is required."],
                    "warna" => ["The warna field is required."],
                    "harga" => ["The harga field is required."],
                    "jenis" => ["The jenis field is required."],
                    "mesin" => ["The mesin field is required."]
                ],
                "data"  => null
            ]);
    }

    public function test_successful_kendaraan_motor()
    {
        $motor = [
            "tahun_keluar"      =>  "2021",
            "warna"             =>  "pink",
            "harga"             =>  2000000,
            "jenis"             =>  "motor",
            "mesin"             =>  "AZ-123",
            "type_suspensi"     =>  "manual",
            "type_transmisi"    =>  "manual",
            "stok"              =>  20
        ];

        $this->json('POST', 'api/kendaraan/store', $motor, ['Accept' => 'application/json'])
            ->assertStatus(200);
    }

    public function test_successful_kendaraan_mobil()
    {
        $motor = [
            "tahun_keluar"          =>  "2021",
            "warna"                 =>  "pink",
            "harga"                 =>  2000000,
            "jenis"                 =>  "mobil",
            "mesin"                 =>  "AZ-124",
            "kapasitas_penumpang"   =>  10,
            "tipe"                  =>  "off road",
            "stok"                  =>  20
        ];

        $this->json('POST', 'api/kendaraan/store', $motor, ['Accept' => 'application/json'])
            ->assertStatus(200);
    }
}
